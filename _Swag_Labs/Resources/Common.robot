*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library	 String

*** Variables ***
${BROWSER}		Chrome
${URL}			https://www.saucedemo.com
${SEARCHTERM}	Ciao

*** Keywords ***
Begin WebTest
    Open Browser  about:blank  gc
    Maximize Browser Window

End WebTest
    Close Browser

Verify All item page
    Wait Until Page Contains  Products
	
Read csv Users
	Log    Read Users.csv 
	${csv}	Get File	C:/_Tech-Challenge/Resources/Users.csv
	@{read}	Create List	${csv}
	@{lines}	Split To Lines	@{read}	1
	
	FOR	${line}	IN	@{lines}
		@{split_line}	Split String	${line}	|
		#${type} =	Set Variable	${split_line}[0]
		Run Keyword If  "${split_line}[0]" == "STD"  ${username} =	Set Variable	${split_line}[1]  
		Log to Console	${line}
		Log to Console	${username}
		Log to Console	${password} 
		
	END
	