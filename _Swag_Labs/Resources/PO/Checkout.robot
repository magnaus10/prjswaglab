*** Settings ***
Library  SeleniumLibrary

*** Keywords ***

Proceed to Checkout
    Click button	id=checkout

Enter Information
	Input Text  id=first-name  Joe
	sleep	1
	Input Text  id=last-name  Smith
	sleep	1
	Input Text  id=postal-code  00044
	sleep	1

Continue Checkout
	Click button	id=continue

Verify Overview page
    Wait Until Page Contains  Overview


Finish Checkout
	Click button	id=finish	

Verify checkout complete
    Wait Until Page Contains  Thank you for your order!
	
Back Home page
    Click button	id=back-to-products
	
