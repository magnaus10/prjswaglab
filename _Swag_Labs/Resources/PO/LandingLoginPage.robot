*** Settings ***
Library  SeleniumLibrary


*** Keywords ***
Load
    Go To	${URL}
   
Verify Page Loaded
    Wait Until Page Contains  Accepted usernames are		10
	
Verify Error in Page Loaded
    Wait Until Page Contains  Epic sadface: Sorry, this user has been locked out.		10