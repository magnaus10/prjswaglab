*** Settings ***
Documentation  Access to Swag Lab 
Library  SeleniumLibrary
Library  OperatingSystem
Library	 String


*** Keywords ***
Access App
	[Arguments]    ${username}   ${password}
    Enter UserPaswd		${username}		${password}
    Submit LoginButton

Enter UserPaswd
	[Arguments]    ${username}   ${password}
    Input Text  id=user-name  ${username}
	sleep	1
	Input Text  id=password  ${password}
	sleep	1
	

Submit LoginButton
    Click Button  xpath=//*[@id="login-button"]
    