*** Settings ***
Library  SeleniumLibrary

Resource  C:/_Swag_Labs/Resources/PO/LandingLoginPage.robot
Resource  C:/_Swag_Labs/Resources/PO/Product.robot
Resource  C:/_Swag_Labs/Resources/PO/Cart.robot
Resource  C:/_Swag_Labs/Resources/PO/LoginSwagLab.robot
Resource  C:/_Swag_Labs/Resources/PO/Checkout.robot  

*** Keywords ***
#access to app
Access to Swag Lab
	[Arguments]    ${username}   ${password}
    LandingLoginPage.Load
	LandingLoginPage.Verify Page Loaded
    LoginSwagLab.Access App		${username}   ${password}

#check product page
correct product page loads
    Product.Verify Page Loaded

#acces to cart
Access to cart
    Cart.Access to cart

#user adds the product to their cart
Add Product to Cart
    Product.Add to Cart

#check Product added to cart
product in cart
	[Arguments]		${text}
    Cart.Verify Product Added	${text}

#user remove the product to their cart and continue shopping
Remove Product in Cart and continue
    Cart.remove to cart and continue

#user attempts to checkout
Proceed to Checkout
    Checkout.Proceed to Checkout

#user enter checkout information
Enter Checkout information
    Checkout.Enter Information
	
#user continue checkout
Continue Checkout
    Checkout.Continue Checkout	

#check checkout overview page
correct checkout overview page loads
    Checkout.Verify Overview page
	
#user Finish Checkout
Finish Checkout
    Checkout.Finish Checkout
	
Verify checkout complete	
	Checkout.Verify checkout complete


Back Home page	
	Checkout.Back Home page