*** Settings ***
Library  SeleniumLibrary
Library  OperatingSystem
Library	 String

Documentation  SwagLab test suite

Resource  ../Resources/Common.robot  # necessary for Setup & Teardown
Resource  ../Resources/SwagLabApp.robot  # necessary for lower level keywords in test cases

Test Setup  Common.Begin WebTest
Test Teardown  Common.End WebTest


# -- Run from command line--
# Copy/paste the below line to Terminal window to execute : 
#
#Run "Access to Swag Lab - Standard User" Test case
#	robot -N "Access to Swag Lab - Standard User" -t "Login Standard User" -d ..\Results -r ReportAccesStandard  -l LogAccesStandard  SwagLabTestSuite.robot
#Run "Access to Swag Lab - Locked User" Test case
#	robot -N "Access to Swag Lab - Locked User" -t "Login Locked User" -d ..\Results -r ReportLockedUser  -l LogLockedUser SwagLabTestSuite.robot
# Run "Add product in Cart" Test case
#	robot -N "Add product in Cart" -i "AddProductCart" -d ..\Results -r ReportAddProductCart  -l LogAddProductCart   SwagLabTestSuite.robot
# Run "Remove product in Cart and Continue shopping" Test case
#	robot -N "Remove product in Cart and Continue shopping" -i "RemoveCartandContinue" -d ..\Results -r ReportRemoveCartandContinue  -l LogRemoveCartRemoveCartandContinue   SwagLab.robot
# Run "Add product in Cart and Checkout" Test case
#	robot -N "Add product in Cart and Checkout" -i "AddCartandCheckout" -d ..\Results -r ReportAddCartandCheckout  -l LogAddCartandCheckout   SwagLabTestSuite.robot
#

*** Variables ***
${PRODUCT}	Sauce Labs Bike Light
${USERNAME}	standard_user
${PASSWORD}	secret_sauce

*** Test Cases ***
Login Standard User
    [Tags]  LoginSTD
    SwagLabApp.Access to Swag Lab	${USERNAME}	${PASSWORD}

Login Locked User
    [Tags]  LoginLCK
    SwagLabApp.Access to Swag Lab	locked_out_user		${PASSWORD}
	

Select product and Add to chart
    [Tags]  AddProductCart
	SwagLabApp.Access to Swag Lab	${USERNAME}	${PASSWORD}
    SwagLabApp.correct product page loads
	SwagLabApp.Add Product to Cart
	SwagLabApp.Access to cart
	SwagLabApp.product in cart		${PRODUCT}
    

Select product and Add to chart and Remove
    [Tags]  RemoveCartandContinue
	SwagLabApp.Access to Swag Lab	${USERNAME}	${PASSWORD}
    SwagLabApp.correct product page loads
	SwagLabApp.Add Product to Cart
	SwagLabApp.Access to cart
	SwagLabApp.product in cart		${PRODUCT}
    SwagLabApp.Remove Product in Cart and continue

Select product and Add to chart and Checkout
    [Tags]  AddCartandCheckout
	SwagLabApp.Access to Swag Lab	${USERNAME}	${PASSWORD}
    SwagLabApp.correct product page loads
	SwagLabApp.Add Product to Cart
	SwagLabApp.Access to cart
	SwagLabApp.product in cart		${PRODUCT}
	SwagLabApp.Proceed to Checkout
	SwagLabApp.Enter Checkout information
	SwagLabApp.Continue Checkout
	SwagLabApp.correct checkout overview page loads
	SwagLabApp.Finish Checkout
    SwagLabApp.Verify checkout complete	
	SwagLabApp.Back Home page
	Common.Verify All item page